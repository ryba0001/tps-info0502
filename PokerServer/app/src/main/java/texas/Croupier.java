package texas;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

public class Croupier implements Serializable{
    private Talon talon;
    private List<Main> mains;
    private List<PlayerHandler> players;
    private int nombreDeJoueurs;
    public List<Carte> cartesSurTable;

    public Croupier(Talon talon, List<PlayerHandler> players) {
        this.talon = talon;
        this.mains = new ArrayList<>();
        this.players = players;
        this.nombreDeJoueurs = players.size();
        this.cartesSurTable = new ArrayList<>();

        // Distribuer les cartes aux joueurs
        for (int i = 0; i < nombreDeJoueurs; i++) {
            List<Carte> cartes = new ArrayList<>();
            cartes.add(talon.piocher());
            cartes.add(talon.piocher());
            Main main = new Main(cartes);
            mains.add(main);
        }

        // Distribuer les cartes sur la table
        for (int i = 0; i < 5; i++) {
            cartesSurTable.add(talon.piocher());
        }
    }

    public List<Main> getMains() {
        return mains;
    }

    public void afficherTable() {
        System.out.println("Affichage des cartes sur la table...");
        for (Carte carte : cartesSurTable) {
            System.out.println(carte);
        }
    }

    public void evaluerMains() {
      System.out.println("Évaluation des mains des joueurs...");
      for (int i = 0; i < nombreDeJoueurs; i++) {
          Main main = mains.get(i);
          List<Carte> toutesLesCartes = new ArrayList<>(main.getCartes());
          toutesLesCartes.addAll(cartesSurTable);
          Main mainComplete = new Main(toutesLesCartes);
          int valeurMain = mainComplete.evaluerMain();
          String combinaison = mainComplete.getCombinaison();
          // switch (combinaison){
          //   case "11" :
          //     combinaison = "VALET";
          //     break;
          //   case "12" :
          //     combinaison = "DAME";
          //     break;
          //   case "13" :
          //     combinaison = "ROI";
          //     break;
          //   default :
          //     combinaison = combinaison;
          //     break;
          // }
          players.get(i).sendMessage("Valeur de la main : " + combinaison);
      }
    }

    public void afficherGagnant() {
        System.out.println("Affichage du gagnant...");
        int gagnant = 0;
        Main mainGagnante = new Main(new ArrayList<>(mains.get(0).getCartes()));
        mainGagnante.getCartes().addAll(cartesSurTable);

        for (int i = 1; i < nombreDeJoueurs; i++) {
            Main mainComplete = new Main(new ArrayList<>(mains.get(i).getCartes()));
            mainComplete.getCartes().addAll(cartesSurTable);

            if (mainComplete.comparerAvec(mainGagnante) > 0) {
                gagnant = i;
                mainGagnante = mainComplete;
            }
        }

        PlayerHandler gagnantHandler = players.get(gagnant);

        // Envoyer un message a chaque joueur en disant s'il a gagné ou non
        for (int i = 0; i < nombreDeJoueurs; i++) {
            if (i == gagnant) {
                players.get(i).sendMessage("Vous avez gagné avec : " + mainGagnante.getCombinaison());
            } else {
                players.get(i).sendMessage("Vous avez perdu. Le gagnant  (" + gagnantHandler.getPseudo() +  ") a : " + mainGagnante.getCombinaison());
            }
        }

        System.out.println("Le gagnant est : " + gagnantHandler.getPseudo() + " avec : " + mainGagnante.getCombinaison());
    }
}