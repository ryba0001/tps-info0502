package texas;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.io.Serializable;

public class Main implements Serializable{

  /* Constantes */
  private static final int HAUTEUR = 1;
  private static final int PAIRE = 2;
  private static final int DOUBLE_PAIRE = 3;
  private static final int BRELAN = 4;
  private static final int SUITE = 5;
  private static final int COULEUR = 6;
  private static final int FULL = 7;
  private static final int CARRE = 8;
  private static final int QUINTE_FLUSH = 9;

  /* Attributs */
  protected List<Carte> cartes;

  /* Constructeur */
  public Main(List<Carte> cartes) {
    this.cartes = new ArrayList<>(cartes);
    trierCartes();
  }

  List<Carte> getCartes() {
    return cartes;
  }

  /* Méthodes */
  // Trier les cartes par ordre décroissant de valeur pour simplifier les
  // comparaisons
  public void trierCartes() {
    this.cartes.sort(Comparator.comparing(c -> c.getValeur().getValeur(), Collections.reverseOrder()));
  }

  public void afficher() {
    for (Carte carte : this.cartes) {
      System.out.println(carte);
    }
  }

  public int evaluerMain() {
      if (estQuinteFlush())
          return QUINTE_FLUSH;
      if (estCarre())
          return CARRE;
      if (estFull())
          return FULL;
      if (estCouleur())
          return COULEUR;
      if (estSuite())
          return SUITE;
      if (estBrelan())
          return BRELAN;
      if (estDoublePaire())
          return DOUBLE_PAIRE;
      if (estPaire())
          return PAIRE;
      return HAUTEUR;
  }

  // Vérification des combinaisons (inchangées)
  public boolean estQuinteFlush() {
    return estSuite() && estCouleur();
  }

  public boolean estCarre() {
    return aNCartesDeMemeValeur(4);
  }

  public boolean estFull() {
    return estBrelan() && estPaire();
  }

  public boolean estCouleur() {
    return cartes.stream().allMatch(c -> c.getCouleur() == cartes.get(0).getCouleur());
}

  public boolean estSuite() {
    List<Integer> valeurs = cartes.stream()
        .map(c -> c.getValeur().getValeur())
        .sorted()
        .distinct()
        .collect(Collectors.toList());

    if (valeurs.size() < 5) {
        return false;
    }

    for (int i = 0; i <= valeurs.size() - 5; i++) {
        if (valeurs.get(i + 4) - valeurs.get(i) == 4) {
            return true;
        }
    }
    return estSuiteAvecAs();
  }

  public boolean estBrelan() {
    return aNCartesDeMemeValeur(3);
  }

  public boolean estDoublePaire() {
    return compterPaires() == 2;
  }

  public boolean estPaire() {
    return aNCartesDeMemeValeur(2);
  }

  private boolean aNCartesDeMemeValeur(int n) {
    for (Carte carte : this.cartes) {
      long count = cartes.stream().filter(c -> c.getValeur() == carte.getValeur()).count();
      if (count == n)
        return true;
    }
    return false;
  }

  private int compterPaires() {
    int count = 0;
    List<Integer> valeursDejaVues = new ArrayList<>();
    for (Carte carte : this.cartes) {
      int valeur = carte.getValeur().getValeur();
      if (!valeursDejaVues.contains(valeur)
          && cartes.stream().filter(c -> c.getValeur().getValeur() == valeur).count() == 2) {
        count++;
        valeursDejaVues.add(valeur);
      }
    }
    return count;
  }

  // Vérifie si c'est une suite, y compris la suite avec As (As, 2, 3, 4, 5)
  private boolean estSuiteAvecAs() {
    List<Integer> valeurs = new ArrayList<>();
    for (Carte carte : this.cartes) {
      valeurs.add(carte.getValeur().getValeur());
    }
    valeurs.sort(Collections.reverseOrder());
    if (valeurs.equals(List.of(14, 5, 4, 3, 2)))
      return true;
    for (int i = 0; i < valeurs.size() - 1; i++) {
      if (valeurs.get(i) - 1 != valeurs.get(i + 1))
        return false;
    }
    return true;
  }

  // Comparer deux mains
  public int comparerAvec(Main autre) {
    int combinaison1 = this.evaluerMain();
    int combinaison2 = autre.evaluerMain();

    if (combinaison1 != combinaison2) {
      return Integer.compare(combinaison1, combinaison2);
    }

    // Si les combinaisons sont égales, on compare les valeurs spécifiques
    switch (combinaison1) {
      case QUINTE_FLUSH:
      case SUITE:
        return comparerCartesHauteValeur(autre);
      case CARRE:
        return comparerNCartesDeMemeValeur(4, autre);
      case FULL:
        return comparerNCartesDeMemeValeur(3, autre);
      case COULEUR:
        return comparerCartesHauteValeur(autre);
      case HAUTEUR:
        return comparerCartesHauteValeur(autre);
      case BRELAN:
        return comparerNCartesDeMemeValeur(3, autre);
      case DOUBLE_PAIRE:
        return comparerDoublePaire(autre);
      case PAIRE:
        return comparerNCartesDeMemeValeur(2, autre);
      default:
        return 0;
    }
  }

  private int comparerCartesHauteValeur(Main autre) {
    for (int i = 0; i < this.cartes.size(); i++) {
      int cmp = Integer.compare(this.cartes.get(i).getValeur().getValeur(),
          autre.cartes.get(i).getValeur().getValeur());
      if (cmp != 0)
        return cmp;
    }
    return 0;
  }

  private int comparerNCartesDeMemeValeur(int n, Main autre) {
    int valeur1 = trouverValeurNCartes(n);
    int valeur2 = autre.trouverValeurNCartes(n);
    if (valeur1 != valeur2)
      return Integer.compare(valeur1, valeur2);
    return comparerCartesHauteValeur(autre);
  }

  private int trouverValeurNCartes(int n) {
    for (Carte carte : this.cartes) {
      long count = cartes.stream().filter(c -> c.getValeur() == carte.getValeur()).count();
      if (count == n)
        return carte.getValeur().getValeur();
    }
    return -1;
  }

  public int comparerDoublePaire(Main autreMain) {
      List<Integer> pairesCetteMain = extrairePaires();
      List<Integer> pairesAutreMain = autreMain.extrairePaires();

      // Trier les paires par ordre décroissant
      pairesCetteMain.sort(Collections.reverseOrder());
      pairesAutreMain.sort(Collections.reverseOrder());

      // Comparer les paires les plus hautes
      for (int i = 0; i < Math.min(pairesCetteMain.size(), pairesAutreMain.size()); i++) {
          int comparaison = pairesCetteMain.get(i).compareTo(pairesAutreMain.get(i));
          if (comparaison != 0) {
              return comparaison;
          }
      }

      // Si les paires sont égales, comparer les cartes restantes
      return comparerCartesRestantes(autreMain);
  }

  private int comparerCartesRestantes(Main autreMain) {
    List<Carte> cartesRestantesCetteMain = getCartesRestantes(extrairePaires());
    List<Carte> cartesRestantesAutreMain = autreMain.getCartesRestantes(autreMain.extrairePaires());

    cartesRestantesCetteMain.sort(Comparator.comparing(Carte::getValeur).reversed());
    cartesRestantesAutreMain.sort(Comparator.comparing(Carte::getValeur).reversed());

    for (int i = 0; i < Math.min(cartesRestantesCetteMain.size(), cartesRestantesAutreMain.size()); i++) {
        int comparaison = cartesRestantesCetteMain.get(i).getValeur().compareTo(cartesRestantesAutreMain.get(i).getValeur());
        if (comparaison != 0) {
            return comparaison;
        }
    }

    return 0;
  }

  private List<Carte> getCartesRestantes(List<Integer> paires) {
    return cartes.stream()
        .filter(carte -> !paires.contains(carte.getValeur().getValeur()))
        .collect(Collectors.toList());
  }

  private List<Integer> extrairePaires() {
    List<Integer> paires = new ArrayList<>();
    List<Integer> valeursDejaVues = new ArrayList<>();
    for (Carte carte : this.cartes) {
      int valeur = carte.getValeur().getValeur();
      if (!valeursDejaVues.contains(valeur)
          && cartes.stream().filter(c -> c.getValeur().getValeur() == valeur).count() == 2) {
        paires.add(valeur);
        valeursDejaVues.add(valeur);
      }
    }
    return paires;
  }

  public String getCombinaison() {
    int combinaison = evaluerMain();
    switch (combinaison) {
      case QUINTE_FLUSH:
        return "Quinte flush";
      case CARRE:
        return "Carré de " + trouverValeurNCartes(4);
      case FULL:
        return "Full aux " + trouverValeurNCartes(3) + " par les " + trouverValeurNCartes(2);
      case COULEUR:
        return "Couleur de " + cartes.get(0).getCouleur();
      case SUITE:
        return "Suite";
      case BRELAN:
        return "Brelan de " + trouverValeurNCartes(3);
      case DOUBLE_PAIRE:
        return "Double paire de " + extrairePaires().get(0) + " et " + extrairePaires().get(1);
      case PAIRE:
        switch (trouverValeurNCartes(2)){
          case 11:
            return "Paire de VALET";
          case 12:
            return "Paire de DAME";
          case 13:
            return "Paire de ROI";
          default:
            return "Paire de " + trouverValeurNCartes(2);
        }
      case HAUTEUR:
        return "Hauteur " + this.cartes.get(0).getValeur();
      default:
        return "Combinaison inconnue";
    }
  }

    public void afficherCombinaison() {
    int combinaison = evaluerMain();
    switch (combinaison) {
      case QUINTE_FLUSH:
        System.out.print("Quinte flush");
        break;
      case CARRE:
        System.out.print("Carré de " + trouverValeurNCartes(4));
        break;
      case FULL:
        System.out.print("Full aux " + trouverValeurNCartes(3) + " par les " + trouverValeurNCartes(2));
        break;
      case COULEUR:
        System.out.print("Couleur");
        break;
      case SUITE:
        System.out.print("Suite");
        break;
      case BRELAN:
        System.out.print("Brelan de " + trouverValeurNCartes(3));
        break;
      case DOUBLE_PAIRE:
        System.out.print("Double paire de " + extrairePaires().get(0) + " et " + extrairePaires().get(1));
        break;
      case PAIRE:
        int v = trouverValeurNCartes(2);
        System.out.println("valeur de la tête de la paire : " + v);
        switch (v){
          case 11:
            System.out.print("Paire de VALET");
            break;
          case 12:
            System.out.print("Paire de DAME");
            break;
          case 13:
            System.out.print("Paire de ROI");
            break;
          default:
            System.out.print("Paire de " + trouverValeurNCartes(2));
            break;
        }
      case HAUTEUR:
        System.out.print("Hauteur " + this.cartes.get(0).getValeur());
        break;
      default:
        System.out.print("Combinaison inconnue");
        break;
    }
  }

  @Override
  public String toString(){
    StringBuilder sb = new StringBuilder();
    for (Carte carte : cartes) {
      sb.append(carte).append(" / ");
    }
    return sb.toString().trim();
  }
}
