package texas;

import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

public class PaquetCarte implements Serializable {

  /* Attributs */
  protected List<Carte> cartes;

  /* Constructeur */
  public PaquetCarte() {
    this.cartes = new ArrayList<Carte>();
    // Création du paquet de cartes complet
    for (Carte.Couleur couleur : Carte.Couleur.values()) {
      for (Carte.Valeur valeur : Carte.Valeur.values()) {
        this.cartes.add(new Carte(couleur, valeur));
      }
    }
  }

  /* Méthodes */
  // Getters
  public List<Carte> getCartes() {
    return cartes;
  }

  // Affichage du paquet de cartes
  public void afficher() {
    for (Carte carte : this.cartes) {
      System.out.println(carte);
    }
  }

  // Affichage du paquet de cartes en colonnes de chaque couleur
  public void afficherParCouleur() {
    List<Carte> piques = new ArrayList<>();
    List<Carte> coeurs = new ArrayList<>();
    List<Carte> carreaux = new ArrayList<>();
    List<Carte> trefles = new ArrayList<>();

    for (Carte carte : this.cartes) {
      switch (carte.getCouleur()) {
        case PIQUE:
          piques.add(carte);
          break;
        case COEUR:
          coeurs.add(carte);
          break;
        case CARREAU:
          carreaux.add(carte);
          break;
        case TREFLE:
          trefles.add(carte);
          break;
      }
    }

    int maxSize = Math.max(Math.max(piques.size(), coeurs.size()), Math.max(carreaux.size(), trefles.size()));
    String format = "%-25s%-25s%-25s%-25s%n";

    System.out.printf(format, "Piques", "Coeurs", "Carreaux", "Trèfles");
    System.out.println(
        "------------------------------------------------------------------------------------------------");
    for (int i = 0; i < maxSize; i++) {
      String piquesStr = i < piques.size() ? piques.get(i).toString() : "";
      String coeursStr = i < coeurs.size() ? coeurs.get(i).toString() : "";
      String carreauxStr = i < carreaux.size() ? carreaux.get(i).toString() : "";
      String treflesStr = i < trefles.size() ? trefles.get(i).toString() : "";
      System.out.printf(format, piquesStr, coeursStr, carreauxStr, treflesStr);
    }
  }

  // Mélange du paquet de cartes
  public void melanger() {
    List<Carte> cartesMelangees = new ArrayList<>();
    while (!this.cartes.isEmpty()) {
      int index = (int) (Math.random() * this.cartes.size());
      cartesMelangees.add(this.cartes.remove(index));
    }
    this.cartes = cartesMelangees;
  }

  // Tirage d'une carte du paquet de cartes
  public Carte tirer() {
    return this.cartes.remove(0);
  }

}