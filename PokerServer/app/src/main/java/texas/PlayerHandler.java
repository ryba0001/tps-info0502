package texas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class PlayerHandler implements Runnable {
    private Socket socket;
    private PokerServer server;
    private PrintWriter out;
    private BufferedReader in;
    private String pseudo;

    public PlayerHandler(Socket socket, PokerServer server) {
        this.socket = socket;
        this.server = server;
        try {
            socket.setSoTimeout(300000); // Timeout de 5 minutes
        } catch (IOException e) {
            System.err.println("Failed to set socket timeout: " + e.getMessage());
        }
    }

    @Override
    public void run() {
        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            out.println("Welcome to the Poker game! \nWrite \"quit\" to quit the game!");

            boolean pseudoUnique = false;
            while (!pseudoUnique) {
                out.println("Please enter your pseudo:");
                String VerifPseudo = in.readLine();
                if (VerifPseudo == null || VerifPseudo.isEmpty()) {
                    out.println("Pseudo cannot be empty. Please try again.");
                    continue;
                }

                // Vérifier si le pseudo est déjà pris
                synchronized (server) {
                    if (server.isPseudoTaken(VerifPseudo)) {
                        out.println("Sorry, the pseudo \"" + VerifPseudo + "\" is already taken. Please choose another one.");
                    } else {
                        this.pseudo = VerifPseudo;
                        out.println("Hello " + this.pseudo + "!");
                        pseudoUnique = true;
                        server.addPlayer(this);
                    }
                }
            }

            System.out.println("New player connected: " + pseudo);

            // Notifier le serveur
            synchronized (server) {
                server.notifyAll();
            }

            // Boucle pour traiter les messages
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println("Received from " + pseudo + ": " + inputLine);

                // Quitter si le joueur tape "quit"
                if (inputLine.equalsIgnoreCase("quit")) {
                    out.println("Bye " + pseudo + ".");
                    break;
                }

                // Diffuser le message aux autres joueurs
                for (PlayerHandler player : server.getPlayers()) {
                    if (player != this) {
                        player.sendMessage(pseudo + ": " + inputLine);
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("Error handling player: " + e.getMessage());
        } finally {
            try {
                if (pseudo != null) {
                    System.out.println("Player disconnected: " + pseudo);
                }
                socket.close();
            } catch (IOException e) {
                System.err.println("Error closing socket for " + pseudo + ": " + e.getMessage());
            }
            synchronized (server) {
                server.getPlayers().remove(this);
                server.notifyAll();
            }
        }
    }

    public void sendMessage(String message) {
        if (out != null) {
            out.println(message);
        }
    }

    public String getPseudo() {
        return this.pseudo;
    }

    public void closeConnection() {
        try {
            if (out != null) {
                out.close();
            }
            if (in != null) {
                in.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}