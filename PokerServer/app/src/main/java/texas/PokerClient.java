package texas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class PokerClient {
    private static final String SERVER_ADDRESS = "10.11.17.40"; // Adresse du serveur
    private static final int SERVER_PORT = 12345;              // Port du serveur

    public void start() {
        try (Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {

            // Thread pour lire les messages du serveur
            Thread serverReader = new Thread(() -> {
                try {
                    String fromServer;
                    while ((fromServer = in.readLine()) != null) {
                        System.out.println("Server: " + fromServer);
                        if (fromServer.equals("Bye.") || fromServer.equals("Server is shutting down. You have been disconnected.")) {
                            System.out.println("Server disconnected.");
                            System.exit(0); // Arrêter si le serveur envoie "Bye." ou message de déconnexion
                        }
                    }
                } catch (IOException e) {
                    System.err.println("Error reading from server: " + e.getMessage());
                }
            });
            serverReader.start(); // Démarrer le thread de lecture serveur

            // Lecture de pseudo et gestion des réponses
            String fromUser;
            boolean validPseudo = false;
            while ((fromUser = stdIn.readLine()) != null && !validPseudo) {
                out.println(fromUser); // Envoyer le pseudo au serveur
                String response = in.readLine(); // Attendre la réponse du serveur

                if (response != null) {
                    System.out.println("Server: " + response);
                    if (response.contains("Hello")) {
                        validPseudo = true;  // Si le serveur a accepté le pseudo
                    }
                }
            }

            // Attendre la fin du thread avant de fermer
            serverReader.join();

        } catch (IOException | InterruptedException e) {
            System.err.println("Client error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        PokerClient client = new PokerClient();
        client.start();

        System.out.println("Saisir son pseudo : ");
    }
}