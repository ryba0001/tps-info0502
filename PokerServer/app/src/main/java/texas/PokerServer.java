package texas;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class PokerServer {
    private static final int PORT = 12345;
    private static final int MIN_PLAYERS = 2;
    private List<PlayerHandler> players;
    private ServerSocket serverSocket;

    public PokerServer() {
        players = new ArrayList<>();
    }

    public void start() {
        try {
            serverSocket = new ServerSocket(PORT);
            System.out.println("Poker server started on port " + PORT);
            this.afficherPlayers();
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    System.out.println("Server shutting down...");
                    for (PlayerHandler player : players) {
                        player.sendMessage("Server is shutting down. You have been disconnected.");
                        player.closeConnection();
                    }
                    serverSocket.close();
                    System.exit(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }));

            while (true) {
                Socket clientSocket = serverSocket.accept();
                PlayerHandler playerHandler = new PlayerHandler(clientSocket, this);
                players.add(playerHandler);
                new Thread(playerHandler).start();

                synchronized (this) {
                    // Attendre que le nombre de joueurs soit suffisant et qu'ils aient tous un pseudo
                    while (players.size() >= MIN_PLAYERS && !tousLesJoueursOntPseudo()) {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt(); // Restore the interrupted status
                            System.out.println("Thread interrupted: " + e.getMessage());
                        }
                    }

                    // Si nous avons au moins 2 joueurs et tous ont un pseudo, attendre 5 secondes avant de démarrer
                    if (players.size() >= MIN_PLAYERS && tousLesJoueursOntPseudo()) {
                        System.out.println("Waiting for 10 seconds to allow more players to join...");
                        long startTime = System.currentTimeMillis();

                        while (System.currentTimeMillis() - startTime < 10000) {
                            // Vérifier si le nombre de joueurs a changé durant ces 5 secondes
                            if (players.size() < MIN_PLAYERS) {
                                System.out.println("Not enough players. Waiting...");
                                break;
                            }
                            // Petite pause pour éviter une boucle trop serrée
                            try {
                                Thread.sleep(100); // Attendre 100 ms avant de réévaluer
                            } catch (InterruptedException e) {
                                Thread.currentThread().interrupt();
                            }
                        }

                        this.afficherPlayers();  // Afficher la liste des joueurs
                        startGame();  // Lancer le jeu après la temporisation
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startGame() {
        System.out.println("Starting the game with " + players.size() + " players.");
        Talon talon = new Talon(1); // Initialiser le talon avec un paquet de cartes
        talon.melanger();
        Croupier croupier = new Croupier(talon, players);

        // Distribuer les cartes aux joueurs
        for (int i = 0; i < players.size(); i++) {
            PlayerHandler player = players.get(i);
            Main main = croupier.getMains().get(i);
            player.sendMessage(player.getPseudo() + ",  vos cartes : " + main);
        }

        // Afficher les cartes sur la table
        croupier.afficherTable();

        // Évaluer les mains des joueurs
        croupier.evaluerMains();

        // Afficher le gagnant
        croupier.afficherGagnant();
    }

    public List<PlayerHandler> getPlayers() {
        return players;
    }

    public boolean tousLesJoueursOntPseudo() {
        for (PlayerHandler player : players) {
            if (player.getPseudo() == null || player.getPseudo().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public synchronized boolean isPseudoTaken(String pseudo) {
        System.out.println("Checking if pseudo '" + pseudo + "' is taken.");
        this.afficherPlayers();  // Affiche les joueurs actuels

        for (PlayerHandler player : players) {
            if(player.getPseudo() == null) {
                return false;
            }
            if (player.getPseudo() != null && player.getPseudo().trim().equalsIgnoreCase(pseudo.trim())) {
                return true;  // Pseudo trouvé, il est pris
            }
        }
        return false;  // Pseudo non trouvé, il est disponible
    }

    public synchronized void addPlayer(PlayerHandler player) {
        if (player != null && !players.contains(player)) {
            players.add(player);
            System.out.println("Player " + player.getPseudo() + " added to the server.");
        }
    }

    public synchronized void afficherPlayers() {
        System.out.println("Current players: ");
        for (PlayerHandler player : players) {
            System.out.println(player.getPseudo());
        }
        System.out.println();
    }
}