# Projet Poker - Client/Serveur

Bienvenue dans le projet Poker, une application en Java permettant de jouer au poker via un système client/serveur.

## Structure du projet

Ce dépôt contient deux parties principales :

1. **PokerServer** : L'application serveur, qui gère les parties de poker, la logique du jeu et la communication avec les clients.
2. **PokerClient** : L'application client, qui représente l'interface et les interactions pour les joueurs.

## Instructions pour l'utilisation

### 1. Déployer le serveur
Pour démarrer une partie, le serveur doit être lancé sur une machine dédiée. Veuillez vous référer à la documentation dans le fichier `PokerServer` pour les détails de configuration.

### 2. Installer le client
Le fichier `PokerClient` est conçu pour être utilisé sur les machines des joueurs. **Vous devez copier ce fichier sur chaque machine participant à la partie**. Cela permet à plusieurs joueurs de se connecter au serveur et de jouer simultanément.

⚠️ **Note importante** : Le fichier `PokerClient` est inclus ici pour simplifier les pushs et la gestion du dépôt sur Git. Lors du déploiement réel, assurez-vous que chaque joueur utilise la dernière version du client en la récupérant depuis ce dépôt.

## Lancer une partie

1. **Configurer le serveur :**
   - Lancez `PokerServer` sur la machine dédiée.
   - Assurez-vous que le serveur est accessible via le réseau.

2. **Configurer les clients :**
   - Copiez le fichier `PokerClient` sur chaque machine des joueurs.
   - Assurez-vous que chaque machine peut communiquer avec le serveur via le réseau.

3. **Connexion des joueurs :**
   - Chaque joueur exécute `PokerClient` et entre les informations de connexion fournies par l'hôte.

## Contributions

Si vous souhaitez contribuer au projet ou signaler des bugs, n'hésitez pas à ouvrir une issue ou à soumettre une pull request.

---

**Auteur** : [RYBAK Thomas]
**Licence** : [3ème année de licence informatique à l'Université de Reims]

```