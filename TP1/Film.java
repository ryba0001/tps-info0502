public class Film extends Media {

  private String realisateur;
  private int annee;

  public Film(String titre, StringBuffer cote, int note, String realisateur, int annee) {
    super(titre, cote, note);
    this.realisateur = realisateur;
    this.annee = annee;
  }

  public Film() {
    super();
    this.realisateur = "Inconnu";
    this.annee = 0;
  }

  public Film(Film f) {
    super(f);
    this.realisateur = f.realisateur;
    this.annee = f.annee;
  }

  public String getRealisateur() {
    return this.realisateur;
  }

  public int getAnnee() {
    return this.annee;
  }

  public void setRealisateur(String realisateur) {
    this.realisateur = realisateur;
  }

  public void setAnnee(int annee) {
    this.annee = annee;

  }

  public String toString() {
    return super.toString() + "\nRealisateur : " + this.realisateur + "\nAnnee : " + this.annee;
  }

  public boolean equals(Film f) {
    return super.equals(f) && this.realisateur.equals(f.realisateur) && this.annee == f.annee;
  }

  public Film clone() {
    return new Film(this);
  }

}