public class Main {
  public static void main(String args[]) {
    Media m = new Media("titre M", new StringBuffer("coteM"), 5);
    Livre l = new Livre("titre L", new StringBuffer("coteL"), 5, "auteurL", 1);
    System.out.println(m.getTitre());
    System.out.println(m.getCote());
    System.out.println(m.getNote());
    System.out.println(l.getAuteur());
    System.out.println(l.getReference());
    System.out.println(m.toString());
    System.out.println(l.toString());
    System.out.println(m.equals(l));
    System.out.println(l.equals(m));
    System.out.println(m.clone());
    System.out.println(l.clone());
    System.out.println(Media.getNom());
    Media.setNom("nom");
    System.out.println(Media.getNom());

    // rajoute le code pour créer un film et utiliser les méthodes
    Film f = new Film("titre F", new StringBuffer("coteF"), 5, "realisateurF", 1);
    System.out.println(f.getRealisateur());
    System.out.println(f.getAnnee());
    System.out.println(f.toString());
    System.out.println(f.equals(l));
    System.out.println(f.equals(f));
    System.out.println(f.clone());

    // Donnez un exemple d’utilisation de la classe Mediatheque
    Mediatheque mediatheque = new Mediatheque();
    mediatheque.add(m);
    mediatheque.add(l);
    mediatheque.add(f);
    System.out.println(mediatheque.toString());

  }
}