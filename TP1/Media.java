public class Media {

  protected String titre;
  protected StringBuffer cote;
  private int note;

  private static String nom = "Médiathèque de l'URCA";

  public Media(String titre, StringBuffer cote, int note) {
    this.titre = titre;
    this.cote = cote;
    this.note = note;
  }

  public Media() {
    this.titre = "Inconnu";
    this.cote = new StringBuffer("Inconnu");
    this.note = 0;
  }

  public Media(Media m) {
    this.titre = m.titre;
    this.cote = m.cote;
    this.note = m.note;
  }

  public String getTitre() {
    return this.titre;
  }

  public String getCote() {
    return this.cote.toString();
  }

  public int getNote() {
    return this.note;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }

  public void setCote(StringBuffer cote) {
    this.cote = cote;
  }

  public void setNote(int note) {
    this.note = note;
  }

  public static void setNom(String nom) {
    Media.nom = nom;
  }

  public static String getNom() {
    return Media.nom;
  }

  public String toString() {
    return "Titre : " + this.titre + "\nCote : " + this.cote + "\nNote : " + this.note;
  }

  public boolean equals(Media m) {
    return this.titre.equals(m.titre) && this.cote.equals(m.cote) && this.note == m.note;
  }

  public Media clone() {
    return new Media(this);
  }

}