package edu.info0502.tp1;

public class Livre extends Media {

  private String auteur;
  private int reference;

  public Livre(String titre, StringBuffer cote, int note, String auteur, int reference) {
    super(titre, cote, note);
    this.auteur = auteur;
    this.reference = reference;
  }

  public Livre() {
    super();
    this.auteur = "Inconnu";
    this.reference = 0;
  }

  public Livre(Livre l) {
    super(l);
    this.auteur = l.auteur;
    this.reference = l.reference;
  }

  public String getAuteur() {
    return this.auteur;
  }

  public int getReference() {
    return this.reference;
  }

  public void setAuteur(String auteur) {
    this.auteur = auteur;
  }

  public void setReference(int reference) {
    this.reference = reference;
  }

  public String toString() {
    return super.toString() + "\nAuteur : " + this.auteur + "\nReference : " + this.reference;
  }

  public boolean equals(Livre l) {
    return super.equals(l) && this.auteur.equals(l.auteur) && this.reference == l.reference;
  }

  public Livre clone() {
    return new Livre(this);
  }

}