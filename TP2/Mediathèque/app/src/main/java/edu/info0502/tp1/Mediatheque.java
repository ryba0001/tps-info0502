package edu.info0502.tp1;

import java.util.Vector;

public class Mediatheque {
  private String nomProprietaire;
  private Vector<Media> medias;

  public Mediatheque() {
    this.nomProprietaire = "Inconnu";
    this.medias = new Vector<Media>();
  }

  public Mediatheque(Mediatheque m) {
    this.nomProprietaire = m.nomProprietaire;
    this.medias = m.medias;
  }

  public void add(Media m) {
    this.medias.add(m);
  }

  public String toString() {
    String s = "Nom du proprietaire : " + this.nomProprietaire + "\n";
    for (Media m : this.medias) {
      System.out.println("Media numéro : " + this.medias.indexOf(m));
      s += m.toString() + "\n";
      s += "\n\n";
    }
    return s;
  }
}