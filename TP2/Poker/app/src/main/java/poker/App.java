package poker;

public class App {

  // Méthode principale
  public static void main(String[] args) {

    // Création d'un talon
    Talon talon = new Talon(1);

    System.out.println("\nAffichage du talon :\n");

    // Affichage du talon en colonnes de chaque couleur
    talon.afficher();

    System.out.println("\nMélange du talon :\n");

    // Mélange du talon
    talon.melanger();

    // Affichage du talon
    talon.afficher();

    // Création d'un croupier
    Croupier croupier = new Croupier(talon, 5);

    System.out.println("\nAffichage de la main 1 :\n");

    // Affichage de la main1
    croupier.getMains().get(0).afficher();

    System.out.println("\nEvaluation de la main 1 :\n");

    // Evaluation de la main1
    croupier.getMains().get(0).afficherCombinaison();

    System.out.println("\nAffichage de la main 2 :\n");

    // Affichage de la main2
    croupier.getMains().get(1).afficher();

    System.out.println("\nEvaluation de la main 2 :\n");

    // Evaluation de la main2
    croupier.getMains().get(1).afficherCombinaison();

    // Affichage de la main 3
    System.out.println("\nAffichage de la main 3 :\n");

    // Affichage de la main3
    croupier.getMains().get(2).afficher();

    // Evaluation de la main 3
    System.out.println("\nEvaluation de la main 3 :\n");

    // Evaluation de la main3
    croupier.getMains().get(2).afficherCombinaison();

    // Affichage de la main 4
    System.out.println("\nAffichage de la main 4 :\n");

    // Affichage de la main4
    croupier.getMains().get(3).afficher();

    // Evaluation de la main 4
    System.out.println("\nEvaluation de la main 4 :\n");

    // Evaluation de la main4
    croupier.getMains().get(3).afficherCombinaison();

    // Affichage de la main 5
    System.out.println("\nAffichage de la main 5 :\n");

    // Affichage de la main5
    croupier.getMains().get(4).afficher();

    // Evaluation de la main 5
    System.out.println("\nEvaluation de la main 5 :\n");

    // Evaluation de la main5
    croupier.getMains().get(4).afficherCombinaison();

    // affichage du gagnaant
    System.out.println("\nAffichage du gagnant :\n");

    // Affichage du gagnant
    croupier.afficherGagnant();

  }

}
