package poker;

public class Carte {

  /* Enumérations */

  public enum Couleur {
    PIQUE, COEUR, CARREAU, TREFLE
  }

  public enum Valeur {
    AS(14),
    DEUX(2),
    TROIS(3),
    QUATRE(4),
    CINQ(5),
    SIX(6),
    SEPT(7),
    HUIT(8),
    NEUF(9),
    DIX(10),
    VALET(11),
    DAME(12),
    ROI(13);

    private final int valeur;

    Valeur(int valeur) {
      this.valeur = valeur;
    }

    public int getValeur() {
      return valeur;
    }

  }

  /* Attributs */

  protected Couleur couleur;
  protected Valeur valeur;

  /* Constructeur */

  // Constructeur par défaut
  public Carte() {
    this.couleur = Couleur.PIQUE;
    this.valeur = Valeur.AS;
  }

  // Constructeur avec paramètres
  public Carte(Couleur couleur, Valeur valeur) {
    this.couleur = couleur;
    this.valeur = valeur;
  }

  // Constructeur par copie
  public Carte(String couleur, String valeur) {
    this.couleur = Couleur.valueOf(couleur);
    this.valeur = Valeur.valueOf(valeur);
  }

  /* Méthodes */

  // Getters
  public Couleur getCouleur() {
    return couleur;
  }

  public Valeur getValeur() {
    return valeur;
  }

  // Setters
  public void setCouleur(Couleur couleur) {
    this.couleur = couleur;
  }

  public void setValeur(Valeur valeur) {
    this.valeur = valeur;
  }

  // Affichage
  public String toString() {
    return valeur + " de " + couleur;
  }

  // Comparaison de deux cartes
  public boolean equals(Carte carte) {
    return this.couleur == carte.couleur && this.valeur == carte.valeur;
  }

  // Supérieur
  public boolean sup(Carte carte) {
    return this.valeur.getValeur() > carte.getValeur().getValeur();
  }

  // Inférieur
  public boolean inf(Carte carte) {
    boolean inf;
    if (this.equals(carte) || this.sup(carte)) {
      inf = false;
    } else {
      inf = true;
    }
    return inf;
  }

}
