package poker;

import java.util.ArrayList;
import java.util.List;

public class Croupier {

  /* Attributs */
  protected List<Main> mains;

  /* Constructeur */
  public Croupier(Talon talon, int nmbJoueur) {
    // Initialisation de la liste de mains pour chaque joueur
    this.mains = new ArrayList<>();

    // Créer une main vide pour chaque joueur
    for (int i = 0; i < nmbJoueur; i++) {
      this.mains.add(new Main(new ArrayList<>()));
    }

    // Distribution des cartes : 5 cartes pour chaque joueur, une par une
    for (int i = 0; i < 5; i++) { // 5 cartes par joueur
      for (int j = 0; j < nmbJoueur; j++) {
        // Distribuer une carte du talon à la main du joueur
        Carte carte = talon.tirer();
        this.mains.get(j).cartes.add(carte);
      }
    }
    // Trier les cartes de chaque main
    for (Main main : this.mains) {
      main.trierCartes();
    }
  }

  // Retourne la liste des mains des joueurs
  public List<Main> getMains() {
    return mains;
  }

  // Affiche le gagnant de la partie
  public void afficherGagnant() {
    List<Integer> gagnants = new ArrayList<>(); // Indices des joueurs gagnants
    int meilleurScore = -1;

    // Parcours de toutes les mains pour trouver la meilleure combinaison
    for (int i = 0; i < mains.size(); i++) {
      int score = mains.get(i).evaluerMain();
      if (score > meilleurScore) {
        meilleurScore = score;
        gagnants.clear(); // Réinitialise la liste des gagnants
        gagnants.add(i); // Ajoute le joueur actuel comme gagnant potentiel
      } else if (score == meilleurScore) {
        // Vérification supplémentaire avec comparerAvec pour les mains ayant le même
        // type de combinaison
        if (!gagnants.isEmpty()) {
          int comparaison = mains.get(gagnants.get(0)).comparerAvec(mains.get(i));
          if (comparaison < 0) {
            // Nouvelle main est supérieure, donc elle devient la seule gagnante
            gagnants.clear();
            gagnants.add(i);
          } else if (comparaison == 0) {
            // Même combinaison et même valeur, on ajoute cette main à la liste des gagnants
            gagnants.add(i);
          }
          // Si comparaison > 0, la main actuelle est inférieure donc on ne l'ajoute pas
        }
      }
    }

    // Affichage du gagnant ou des gagnants en cas d'égalité
    if (gagnants.size() == 1) {
      System.out.print("Le joueur " + (gagnants.get(0) + 1) + " a gagné avec une combinaison de ");
      mains.get(gagnants.get(0)).afficherCombinaison();
      System.out.println(" !");
    } else {
      System.out.print("Égalité entre les joueurs ");
      for (int i = 0; i < gagnants.size(); i++) {
        System.out.print((gagnants.get(i) + 1));
        if (i < gagnants.size() - 1) {
          System.out.print(", ");
        }
      }
      System.out.print(" avec une combinaison de ");
      mains.get(gagnants.get(0)).afficherCombinaison();
      System.out.println(" !");
    }
  }
}
