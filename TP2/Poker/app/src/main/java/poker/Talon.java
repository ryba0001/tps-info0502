package poker;

import java.util.List;
import java.util.ArrayList;

public class Talon {

  /* Attributs */
  protected List<PaquetCarte> paquets;

  private boolean melange = false;

  /* Constructeur */
  public Talon(int nbPaquets) {
    this.paquets = new ArrayList<PaquetCarte>();
    for (int i = 0; i < nbPaquets; i++) {
      this.paquets.add(new PaquetCarte());
    }
  }

  /* Méthodes */

  // Affichage du talon
  public void afficher() {
    if (this.melange == true) {
      for (PaquetCarte paquet : this.paquets) {
        paquet.afficher();
      }

    } else {
      for (PaquetCarte paquet : this.paquets) {
        paquet.afficherParCouleur();
      }
    }
  }

  // Mélange du talon
  public void melanger() {
    this.melange = true;
    for (PaquetCarte paquet : this.paquets) {
      paquet.melanger();
    }
  }

  // Tirage de la carte du dessus du talon
  public Carte tirer() {
    return this.paquets.get(0).tirer();
  }

}
