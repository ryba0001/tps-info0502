package texas;

public class App {

    // Méthode principale
    public static void main(String[] args) {

        // Création d'un talon
        Talon talon = new Talon(2);

        System.out.println("\nAffichage du talon :\n");

        // Affichage du talon en colonnes de chaque couleur
        talon.afficher();

        System.out.println("\nMélange du talon :\n");

        // Mélange du talon
        talon.melanger();

        // Affichage du talon
        talon.afficher();

        // Création d'un croupier
        Croupier croupier = new Croupier(talon, 5);

        System.out.println("\nAffichage de la table :\n");

        // Affichage de la table
        croupier.afficherTable();

        System.out.println("\nEvaluation des mains :\n");

        // Evaluation des mains
        croupier.evaluerMains();

        // affichage du gagnaant
        System.out.println("\nAffichage du gagnant :\n");

        // Affichage du gagnant
        croupier.afficherGagnant();

    }

}
