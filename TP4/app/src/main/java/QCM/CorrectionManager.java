package QCM;

import java.util.Map;

public class CorrectionManager {
    private QCMManager qcmManager;

    public CorrectionManager(QCMManager qcmManager) {
        this.qcmManager = qcmManager;
    }

    /**
     * Corrige un QCM et retourne le score obtenu.
     *
     * @param qcmId     L'identifiant du QCM.
     * @param reponses  Les réponses fournies par l'utilisateur, mapping questionId -> réponse utilisateur.
     * @return Le score obtenu (nombre de réponses correctes), ou -1 si le QCM n'existe pas.
     */
    public int corrigerQCM(String qcmId, Map<String, String> reponses) {
        QCM qcm = qcmManager.getQCM(qcmId);
        if (qcm == null) {
            return -1; // QCM introuvable
        }

        int score = 0;
        for (Question question : qcm.getQuestions()) {
            String reponseUser = reponses.get(question.getQuestionId());
            if (reponseUser != null && question.getReponseCorrecte().equalsIgnoreCase(reponseUser)) {
                score++;
            }
        }
        return score;
    }
}
