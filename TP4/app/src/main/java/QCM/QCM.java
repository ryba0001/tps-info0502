package QCM;

import java.util.List;

public class QCM {
    private String qcmId;
    private String titre;
    private List<Question> questions;
    public QCM() {}
    public String getQcmId() {
        return qcmId;
    }
    public void setQcmId(String qcmId) {
        this.qcmId = qcmId;
    }
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }
    public List<Question> getQuestions() {
        return questions;
    }
    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}