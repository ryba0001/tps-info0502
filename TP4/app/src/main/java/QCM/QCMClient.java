package QCM;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class QCMClient {
    private final static String REQUEST_QUEUE = "qcm_requests";
    private final static String RESPONSE_QUEUE = "qcm_responses";
    private ObjectMapper mapper;
    private Connection connection;
    private Channel channel;
    private String response;
    private final Object monitor = new Object();
    private QCM currentQCM;

    // Configuration RabbitMQ
    private final static String RABBITMQ_HOST = "localhost";
    private final static String RABBITMQ_USERNAME = "thomas";
    private final static String RABBITMQ_PASSWORD = "thomas";

    public QCMClient() throws IOException, TimeoutException {
        mapper = new ObjectMapper();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(RABBITMQ_HOST);
        factory.setUsername(RABBITMQ_USERNAME);
        factory.setPassword(RABBITMQ_PASSWORD);

        connection = factory.newConnection();
        channel = connection.createChannel();

        // Déclarer les queues si elles n'existent pas
        channel.queueDeclare(REQUEST_QUEUE, false, false, false, null);
        channel.queueDeclare(RESPONSE_QUEUE, false, false, false, null);

        // Consommer les réponses
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                synchronized (monitor) {
                    response = new String(body, StandardCharsets.UTF_8);
                    monitor.notify();
                }
            }
        };
        channel.basicConsume(RESPONSE_QUEUE, true, consumer);
    }

    public String sendRequest(String message) throws IOException {
        channel.basicPublish("", REQUEST_QUEUE, null, message.getBytes(StandardCharsets.UTF_8));
        // Attendre la réponse
        synchronized (monitor) {
            try {
                monitor.wait(5000); // Timeout de 5 secondes
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    public void close() throws IOException, TimeoutException {
        connection.close();
    }

    public void start() throws IOException {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\n=== Gestionnaire de QCM ===");
            System.out.println("1. Inscription");
            System.out.println("2. Demander un QCM");
            System.out.println("3. Répondre à un QCM");
            System.out.println("4. Quitter");
            System.out.print("Choix: ");
            String choix = scanner.nextLine();

            try {
                switch (choix) {
                    case "1":
                        handleInscription(scanner);
                        break;
                    case "2":
                        handleDemandeQCM(scanner);
                        break;
                    case "3":
                        handleReponseQCM(scanner);
                        break;
                    case "4":
                        System.out.println("Au revoir !");
                        return;
                    default:
                        System.out.println("Choix invalide.");
                }
            } catch (IOException e) {
                System.out.println("Erreur lors de l'envoi de la requête.");
                e.printStackTrace();
            }
        }
    }

    private void handleInscription(Scanner scanner) throws IOException {
        System.out.print("Nom d'utilisateur: ");
        String username = scanner.nextLine();
        System.out.print("Mot de passe: ");
        String password = scanner.nextLine();

        Map<String, Object> request = new HashMap<>();
        request.put("action", "inscription");
        Map<String, String> data = new HashMap<>();
        data.put("username", username);
        data.put("password", password);
        request.put("data", data);

        String message = mapper.writeValueAsString(request);
        String response = sendRequest(message);
        System.out.println("Réponse: " + response);
    }

    private void handleDemandeQCM(Scanner scanner) throws IOException {
        // Demander la liste des QCMs
        Map<String, Object> listRequest = new HashMap<>();
        listRequest.put("action", "liste_qcm");
        listRequest.put("data", new HashMap<>()); // Pas de données

        String listMessage = mapper.writeValueAsString(listRequest);
        String listResponse = sendRequest(listMessage);

        JsonNode listResponseNode = mapper.readTree(listResponse);
        if (listResponseNode.get("status").asText().equals("success")) {
            JsonNode qcmsNode = listResponseNode.get("qcms");
            List<QCMInfo> qcmInfos = mapper.convertValue(qcmsNode,
                    mapper.getTypeFactory().constructCollectionType(List.class, QCMInfo.class));
            if (qcmInfos.isEmpty()) {
                System.out.println("Aucun QCM disponible.");
                return;
            }

            System.out.println("\n=== Liste des QCMs Disponibles ===");
            for (int i = 0; i < qcmInfos.size(); i++) {
                QCMInfo qcmInfo = qcmInfos.get(i);
                System.out.println((i + 1) + ". " + qcmInfo.getTitre() + " (ID: " + qcmInfo.getQcmId() + ")");
            }

            System.out.print("Choisissez un QCM (numéro): ");
            String choixStr = scanner.nextLine();
            int choixNum;
            try {
                choixNum = Integer.parseInt(choixStr);
                if (choixNum < 1 || choixNum > qcmInfos.size()) {
                    System.out.println("Numéro invalide.");
                    return;
                }
            } catch (NumberFormatException e) {
                System.out.println("Entrée invalide.");
                return;
            }

            QCMInfo selectedQCM = qcmInfos.get(choixNum - 1);
            currentQCM = fetchQCM(scanner, selectedQCM.getQcmId());

        } else {
            System.out.println("Erreur lors de la récupération des QCMs: " + listResponseNode.get("message").asText());
        }
    }

    private QCM fetchQCM(Scanner scanner, String qcmId) throws IOException {
        System.out.print("Nom d'utilisateur: ");
        String username = scanner.nextLine();
        System.out.print("Mot de passe: ");
        String password = scanner.nextLine();

        Map<String, Object> request = new HashMap<>();
        request.put("action", "demande_qcm");
        Map<String, String> data = new HashMap<>();
        data.put("username", username);
        data.put("password", password);
        data.put("qcmId", qcmId);
        request.put("data", data);

        String message = mapper.writeValueAsString(request);
        String response = sendRequest(message);

        JsonNode responseNode = mapper.readTree(response);
        if (responseNode.get("status").asText().equals("success")) {
            JsonNode qcmNode = responseNode.get("qcm");
            QCM qcm = mapper.treeToValue(qcmNode, QCM.class);
            System.out.println("\n=== QCM: " + qcm.getTitre() + " ===");
            for (Question question : qcm.getQuestions()) {
                System.out.println("\nQuestion " + question.getQuestionId() + ": " + question.getTexte());
                int optionNumber = 1;
                for (String option : question.getOptions()) {
                    System.out.println(optionNumber + ". " + option);
                    optionNumber++;
                }
            }
            return qcm;
        } else {
            System.out.println("Erreur: " + responseNode.get("message").asText());
            return null;
        }
    }

    private void handleReponseQCM(Scanner scanner) throws IOException {
        if (currentQCM == null) {
            System.out.println("Vous devez d'abord demander un QCM avant de pouvoir y répondre.");
            return;
        }

        System.out.print("Nom d'utilisateur: ");
        String username = scanner.nextLine();
        System.out.print("Mot de passe: ");
        String password = scanner.nextLine();
        String qcmId = currentQCM.getQcmId();

        Map<String, String> reponses = new HashMap<>();
        for (Question question : currentQCM.getQuestions()) {
            System.out.println("\n" + question.getTexte());
            int optionNumber = 1;
            for (String option : question.getOptions()) {
                System.out.println(optionNumber + ". " + option);
                optionNumber++;
            }
            System.out.print("Votre réponse (numéro de l'option): ");
            String choixOption = scanner.nextLine();

            int choixInt;
            try {
                choixInt = Integer.parseInt(choixOption);
                if (choixInt < 1 || choixInt > question.getOptions().size()) {
                    System.out.println("Option invalide. La réponse sera considérée comme incorrecte.");
                    reponses.put(question.getQuestionId(), "Invalid");
                    continue;
                }
            } catch (NumberFormatException e) {
                System.out.println("Entrée invalide. La réponse sera considérée comme incorrecte.");
                reponses.put(question.getQuestionId(), "Invalid");
                continue;
            }

            String reponse = question.getOptions().get(choixInt - 1);
            reponses.put(question.getQuestionId(), reponse);
        }

        Map<String, Object> request = new HashMap<>();
        request.put("action", "reponse_qcm");
        Map<String, Object> data = new HashMap<>();
        data.put("username", username);
        data.put("password", password);
        data.put("qcmId", qcmId);
        data.put("reponses", reponses);
        request.put("data", data);

        String message = mapper.writeValueAsString(request);
        String response = sendRequest(message);
        System.out.println("Réponse: " + response);

        currentQCM = null;
    }

    public static void main(String[] args) {
        try {
            QCMClient client = new QCMClient();
            client.start();
            client.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public static class QCMInfo {
        private String qcmId;
        private String titre;

        public QCMInfo() {}

        public String getQcmId() {
            return qcmId;
        }

        public void setQcmId(String qcmId) {
            this.qcmId = qcmId;
        }

        public String getTitre() {
            return titre;
        }

        public void setTitre(String titre) {
            this.titre = titre;
        }
    }
}
