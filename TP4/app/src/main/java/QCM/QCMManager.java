package QCM;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class QCMManager {
    private Map<String, QCM> qcms;
    private ObjectMapper mapper;

    public QCMManager() {
        mapper = new ObjectMapper();
        qcms = new HashMap<>();
        loadQCMs();
    }

    /**
     * Charge les QCM à partir du fichier qcms.json présent dans le classpath.
     */
    private void loadQCMs() {
        try (InputStream is = getClass().getClassLoader().getResourceAsStream("qcms.json")) {
            if (is == null) {
                System.err.println("Le fichier qcms.json n'a pas été trouvé dans le classpath.");
                return;
            }
            List<QCM> qcmList = mapper.readValue(is, new TypeReference<List<QCM>>(){});
            for (QCM qcm : qcmList) {
                qcms.put(qcm.getQcmId(), qcm);
                System.out.println("QCM chargé : ID = " + qcm.getQcmId() + ", Titre = " + qcm.getTitre());
            }
            System.out.println("Total de QCMs chargés : " + qcms.size());
        } catch (IOException e) {
            System.err.println("Erreur lors du chargement des QCMs : " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Retourne le QCM correspondant à l'ID donné.
     *
     * @param qcmId l'identifiant du QCM
     * @return le QCM si trouvé, sinon null
     */
    public QCM getQCM(String qcmId) {
        return qcms.get(qcmId);
    }

    /**
     * Retourne la liste de tous les QCM chargés.
     *
     * @return liste des QCM
     */
    public List<QCM> getAllQCMs() {
        return List.copyOf(qcms.values());
    }
}
