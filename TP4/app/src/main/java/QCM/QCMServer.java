package QCM;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class QCMServer {
    private final static String REQUEST_QUEUE = "qcm_requests";
    private final static String RESPONSE_QUEUE = "qcm_responses";
    private UserManager userManager;
    private QCMManager qcmManager;
    private CorrectionManager correctionManager;
    private ObjectMapper mapper;

    // Ajustez la configuration RabbitMQ si nécessaire
    private final static String RABBITMQ_HOST = "localhost";
    private final static String RABBITMQ_USERNAME = "admin";
    private final static String RABBITMQ_PASSWORD = "adminadmin";

    public QCMServer() {
        userManager = new UserManager();
        qcmManager = new QCMManager();
        correctionManager = new CorrectionManager(qcmManager);
        mapper = new ObjectMapper();
    }

    public void start() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(RABBITMQ_HOST);
        factory.setUsername(RABBITMQ_USERNAME);
        factory.setPassword(RABBITMQ_PASSWORD);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(REQUEST_QUEUE, false, false, false, null);
        channel.queueDeclare(RESPONSE_QUEUE, false, false, false, null);

        System.out.println(" [*] Serveur QCM en attente des messages...");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(" [x] Reçu : " + message);
            handleMessage(message, channel);
        };

        channel.basicConsume(REQUEST_QUEUE, true, deliverCallback, consumerTag -> { });
    }

    private void handleMessage(String message, Channel channel) {
        try {
            JsonNode root = mapper.readTree(message);
            String action = root.get("action").asText();
            JsonNode data = root.get("data");
            Map<String, Object> response = new HashMap<>();

            switch (action) {
                case "inscription":
                    handleInscription(data, response);
                    break;

                case "liste_qcm":
                    handleListeQCM(response);
                    break;

                case "demande_qcm":
                    handleDemandeQCM(data, response);
                    break;

                case "reponse_qcm":
                    handleReponseQCM(data, response);
                    break;

                default:
                    response.put("status", "error");
                    response.put("message", "Action inconnue.");
            }

            String responseMessage = mapper.writeValueAsString(response);
            channel.basicPublish("", RESPONSE_QUEUE, null, responseMessage.getBytes(StandardCharsets.UTF_8));
            System.out.println(" [x] Répondu : " + responseMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleInscription(JsonNode data, Map<String, Object> response) {
        String username = data.get("username").asText();
        String password = data.get("password").asText();
        boolean inscrit = userManager.addUser(username, password);
        if (inscrit) {
            response.put("status", "success");
            response.put("message", "Utilisateur inscrit avec succès.");
        } else {
            response.put("status", "error");
            response.put("message", "Nom d'utilisateur déjà existant.");
        }
    }

    private void handleListeQCM(Map<String, Object> response) {
        List<Map<String, String>> qcmList = new ArrayList<>();
        for (QCM qcm : qcmManager.getAllQCMs()) {
            Map<String, String> qcmInfo = new HashMap<>();
            qcmInfo.put("qcmId", qcm.getQcmId());
            qcmInfo.put("titre", qcm.getTitre());
            qcmList.add(qcmInfo);
        }
        response.put("status", "success");
        response.put("qcms", qcmList);
        System.out.println(" [x] Liste des QCMs envoyée au client : " + qcmList.size() + " QCM(s)");
    }

    private void handleDemandeQCM(JsonNode data, Map<String, Object> response) {
        String username = data.get("username").asText();
        String password = data.get("password").asText();
        String qcmId = data.get("qcmId").asText();

        if (!userManager.authenticate(username, password)) {
            response.put("status", "error");
            response.put("message", "Utilisateur non authentifié.");
        } else {
            QCM qcm = qcmManager.getQCM(qcmId);
            if (qcm != null) {
                response.put("status", "success");
                response.put("qcm", qcm);
            } else {
                response.put("status", "error");
                response.put("message", "QCM non trouvé.");
            }
        }
    }

    private void handleReponseQCM(JsonNode data, Map<String, Object> response) {
        String username = data.get("username").asText();
        String password = data.get("password").asText();
        String qcmId = data.get("qcmId").asText();
        JsonNode reponsesNode = data.get("reponses");
        Map<String, String> reponses = new HashMap<>();
        reponsesNode.fieldNames().forEachRemaining(key -> {
            reponses.put(key, reponsesNode.get(key).asText());
        });

        if (!userManager.authenticate(username, password)) {
            response.put("status", "error");
            response.put("message", "Utilisateur non authentifié.");
            return;
        }

        int score = correctionManager.corrigerQCM(qcmId, reponses);
        if (score >= 0) {
            userManager.updateScore(username, score);
            response.put("status", "success");
            response.put("score", score);
        } else {
            response.put("status", "error");
            response.put("message", "Erreur lors de la correction.");
        }
    }

    public static void main(String[] args) {
        QCMServer server = new QCMServer();
        try {
            server.start();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }
}
