package QCM;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UserManager {
    private Map<String, User> users;
    private ObjectMapper mapper;
    private static final String USERS_FILE = "src/main/resources/users.json";

    public UserManager() {
        mapper = new ObjectMapper();
        users = new HashMap<>();
        loadUsers();
    }

    /**
     * Charge les utilisateurs depuis le fichier JSON.
     * S'il n'existe pas, il est créé.
     */
    private void loadUsers() {
        File file = new File(USERS_FILE);
        if (!file.exists()) {
            // Fichier non trouvé, création d'un nouveau map vide et sauvegarde
            users = new HashMap<>();
            saveUsers();
            System.out.println("users.json non trouvé. Création d'un nouveau fichier.");
            return;
        }

        try {
            users = mapper.readValue(file, new TypeReference<Map<String, User>>() {});
            System.out.println("Utilisateurs chargés avec succès. Total utilisateurs : " + users.size());
        } catch (IOException e) {
            System.err.println("Erreur lors du chargement des utilisateurs : " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Sauvegarde les utilisateurs dans le fichier JSON.
     */
    private void saveUsers() {
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(USERS_FILE), users);
            System.out.println("Utilisateurs sauvegardés avec succès.");
        } catch (IOException e) {
            System.err.println("Erreur lors de la sauvegarde des utilisateurs : " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Ajoute un nouvel utilisateur.
     * @param username le nom d'utilisateur
     * @param password le mot de passe
     * @return true si l'utilisateur est créé, false si l'utilisateur existe déjà
     */
    public boolean addUser(String username, String password) {
        if (users.containsKey(username)) {
            return false; // L'utilisateur existe déjà
        }
        User user = new User(username, password);
        users.put(username, user);
        saveUsers();
        return true;
    }

    /**
     * Authentifie un utilisateur.
     * @param username le nom d'utilisateur
     * @param password le mot de passe
     * @return true si l'authentification réussit, false sinon
     */
    public boolean authenticate(String username, String password) {
        if (!users.containsKey(username)) {
            return false;
        }
        return users.get(username).getPassword().equals(password);
    }

    /**
     * Met à jour le score de l'utilisateur.
     * @param username le nom d'utilisateur
     * @param score le score à ajouter au score existant
     */
    public void updateScore(String username, int score) {
        if (users.containsKey(username)) {
            User user = users.get(username);
            user.setScore(user.getScore() + score);
            saveUsers();
        }
    }
}
